import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderSideMenuComponent } from '../components/header-side-menu/header-side-menu.component';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuard } from '../models/auth.guard';
import { TokenInterceptorService } from '../service/app/token-interceptor.service';

@NgModule({
  declarations: [HeaderSideMenuComponent],
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    IonicModule.forRoot(),
  ],
  exports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    IonicModule,
    HeaderSideMenuComponent,
  ],
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
})
export class SharedModule {}
