import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public rootUrl: string = 'http://192.168.100.17:3000/';
  public userRole: any;
  public currentUserRole: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public getUserRole: Observable<any> = this.currentUserRole.asObservable();

  constructor(
    private http: HttpClient, 
    private router: Router,
    private menu: MenuController,
    private loadingCtrl: LoadingController
  ) { }

  async showLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'Loading...',
      duration: 2000,
      spinner: 'circles',
    });

    loading.present();
  }

  setCurrentUserRole(userRole: any) {
      this.currentUserRole.next(userRole);
  }

  getUser(): Observable<any> {
    return this.http.get(this.rootUrl + 'user-list');
  }

  userLogin(data: any): Observable<any> {
    return this.http.post(this.rootUrl + 'auth/login', data);
  }

  userRegister(data: any): Observable<any> {
    return this.http.post(this.rootUrl + 'auth/register', data);
  }

  loggedIn() {
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

  navigateTo(routeData: string) {
    setTimeout(() => this.router.navigate([routeData]), 0);
    this.menu.close();
  }
}
