import { Component, OnInit } from '@angular/core';
import { AppService } from './service/app/app.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public routeList: any = [
    { id: 1, 
      name: 'User', 
      icon: 'person-outline',
      isToggle: true,
      isOpen: false,
      role: 'user',
      children: [
        {
          id: 1,
          path: '/user-information',
          name: 'User Profile',
          icon: 'person-outline'
        },
        {
          id: 2,
          path: '/consultant-list',
          name: 'Consultant List',
          icon: 'person-outline'
        },
        {
          id: 3,
          path: '/timeline',
          name: 'Timeline',
          icon: 'person-outline',
          hasNotification: true
        },
        {
          id: 4,
          path: '/chat',
          name: 'Chat',
          icon: 'person-outline',
          hasNotification: true
        },
        {
          id: 5,
          path: '/home',
          name: 'Logout',
          icon: 'person-outline'
        }
      ]
    },
    {
      id: 2,
      name: 'Consultant',
      icon: 'people-outline',
      isToggle: true,
      isOpen: false,
      role: 'consultant',
      children: [
        {
          id: 1,
          path: '/consultant-information',
          name: 'Consultant Profile',
          icon: 'person-outline'
        },
        {
          id: 2,
          path: '/consultant-list',
          name: 'Consultant List',
          icon: 'person-outline'
        },
        {
          id: 3,
          path: '/user-list',
          name: 'User List',
          icon: 'person-outline'
        },
        {
          id: 4,
          path: '/timeline',
          name: 'Timeline',
          icon: 'person-outline',
          hasNotification: true
        },
        {
          id: 5,
          path: '/chat',
          name: 'Chat',
          icon: 'person-outline',
          hasNotification: true
        },
        {
          id: 6,
          path: '/home',
          name: 'Logout',
          icon: 'person-outline'
        }
      ]
    },
    {
      id: 3,
      name: 'Admin',
      icon: 'settings-outline',
      isToggle: true,
      isOpen: false,
      role: 'admin',
      children: [
        {
          id: 1,
          path: '/admin-information',
          name: 'Admin Profile',
          icon: 'person-outline'
        },
        {
          id: 2,
          path: '/admin-list',
          name: 'Admin List',
          icon: 'person-outline'
        },
        {
          id: 3,
          path: '/user-list',
          name: 'User List',
          icon: 'person-outline'
        },
        {
          id: 4,
          path: '/consultant-list',
          name: 'Consultant List',
          icon: 'person-outline'
        },
        {
          id: 5,
          path: '/system-maintenance',
          name: 'System Maintenance',
          icon: 'person-outline'
        },
        {
          id: 6,
          path: '/home',
          name: 'Logout',
          icon: 'person-outline'
        }
      ]
    },
  ];

  constructor(private appService: AppService) {}

  public ngOnInit() {
    this.appService.getUserRole.subscribe(data => {
      if (data) {
        this.appService.userRole = data.role;
      } else {
        this.appService.userRole = null;
      }
    });
  }
}
