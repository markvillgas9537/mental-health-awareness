import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-header-side-menu',
  templateUrl: './header-side-menu.component.html',
  styleUrls: ['./header-side-menu.component.scss'],
})
export class HeaderSideMenuComponent implements OnInit {
  @Input() public routeList: any;
  public userRole: any = {};

  constructor(
    private router: Router,
    private menu: MenuController,
    public appService: AppService) { }

  ngOnInit() {
    this.appService.setCurrentUserRole(JSON.parse(JSON.parse(JSON.stringify(localStorage.getItem('userRole')))));
  }

  navigateTo(routeData: string) {
    if (routeData === '/home') {
      this.back();
      this.menu.close();
    } else {
      setTimeout(() => this.router.navigate([routeData]), 0);
      this.menu.close();
      this.resetRouteList();
    }
  }

  toggleNav(index: any) {
    this.routeList[index].isOpen = !this.routeList[index].isOpen;
  }

  resetRouteList() {
    this.routeList.filter(r => r.isOpen = false);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/user-login']);
  }

  back() {
    setTimeout(() => this.router.navigate(['/home']), 0);
    localStorage.removeItem('userRole');
    this.appService.setCurrentUserRole(JSON.parse(JSON.parse(JSON.stringify(localStorage.getItem('userRole')))));
  }

  onMenuChange() {
  }
}
