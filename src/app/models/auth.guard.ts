import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { AppService } from '../service/app/app.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private appService: AppService, private router: Router) {}

  canActivate(): any {
    if (this.appService.loggedIn()) {
      return true
    } else {
      this.router.navigate(['/user-login']);
      return false;
    }
  }
  
}
