import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './models/auth.guard';

const routes: Routes = [
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'user-login',
    loadChildren: () => import('./page/user-login/user-login.module').then( m => m.UserLoginPageModule)
  },
  {
    path: 'admin-login',
    loadChildren: () => import('./page/admin-login/admin-login.module').then( m => m.AdminLoginPageModule)
  },
  {
    path: 'consultant-login',
    loadChildren: () => import('./page/consultant-login/consultant-login.module').then( m => m.ConsultantLoginPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./page/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'user-signup',
    loadChildren: () => import('./page/user-signup/user-signup.module').then( m => m.UserSignupPageModule)
  },
  {
    path: 'user-information',
    loadChildren: () => import('./page/user-information/user-information.module').then( m => m.UserInformationPageModule)
  },
  {
    path: 'admin-list',
    loadChildren: () => import('./page/admin-list/admin-list.module').then( m => m.AdminListPageModule)
  },
  {
    path: 'user-list',
    loadChildren: () => import('./page/user-list/user-list.module').then( m => m.UserListPageModule),
    // canActivate: [AuthGuard]
  },
  {
    path: 'consultant-list',
    loadChildren: () => import('./page/consultant-list/consultant-list.module').then( m => m.ConsultantListPageModule)
  },
  {
    path: 'consultant-signup',
    loadChildren: () => import('./page/consultant-signup/consultant-signup.module').then( m => m.ConsultantSignupPageModule)
  },
  {
    path: 'consultant-information',
    loadChildren: () => import('./page/consultant-information/consultant-information.module').then( m => m.ConsultantInformationPageModule)
  },
  {
    path: 'user-page',
    loadChildren: () => import('./page/user-page/user-page.module').then( m => m.UserPagePageModule)
  },
  {
    path: 'consultant-page',
    loadChildren: () => import('./page/consultant-page/consultant-page.module').then( m => m.ConsultantPagePageModule)
  },
  {
    path: 'admin-page',
    loadChildren: () => import('./page/admin-page/admin-page.module').then( m => m.AdminPagePageModule)
  },
  {
    path: 'timeline',
    loadChildren: () => import('./page/timeline/timeline.module').then( m => m.TimelinePageModule)
  },
  {
    path: 'admin-information',
    loadChildren: () => import('./page/admin-information/admin-information.module').then( m => m.AdminInformationPageModule)
  },
  {
    path: 'system-maintenance',
    loadChildren: () => import('./page/system-maintenance/system-maintenance.module').then( m => m.SystemMaintenancePageModule)
  },  {
    path: 'chat',
    loadChildren: () => import('./page/chat/chat.module').then( m => m.ChatPageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
