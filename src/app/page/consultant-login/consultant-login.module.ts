import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsultantLoginPageRoutingModule } from './consultant-login-routing.module';

import { ConsultantLoginPage } from './consultant-login.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsultantLoginPageRoutingModule,
    SharedModule
  ],
  declarations: [ConsultantLoginPage]
})
export class ConsultantLoginPageModule {}
