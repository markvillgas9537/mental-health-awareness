import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultantLoginPage } from './consultant-login.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultantLoginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultantLoginPageRoutingModule {}
