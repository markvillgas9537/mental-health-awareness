import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-consultant-login',
  templateUrl: './consultant-login.page.html',
  styleUrls: ['./consultant-login.page.scss'],
})
export class ConsultantLoginPage implements OnInit {

  constructor(private router: Router, private appService: AppService) { }

  ngOnInit() {
  }

  login() {
    this.appService.showLoading();
    localStorage.setItem('userRole', JSON.stringify({ role: 'consultant' }));
    this.appService.setCurrentUserRole(JSON.parse(JSON.parse(JSON.stringify(localStorage.getItem('userRole')))));
    this.router.navigate(['/timeline']);
  }

}
