import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserSignupPageRoutingModule } from './user-signup-routing.module';

import { UserSignupPage } from './user-signup.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UserSignupPageRoutingModule
  ],
  declarations: [UserSignupPage]
})
export class UserSignupPageModule {}
