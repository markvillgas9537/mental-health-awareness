import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.page.html',
  styleUrls: ['./user-signup.page.scss'],
})
export class UserSignupPage implements OnInit {
  public userForm: FormGroup; 

  constructor(private appService: AppService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      firstName: ["", Validators.required],
      middleName: [""],
      lastName: ["", Validators.required],
      emailAddress: ["", Validators.required],
      contactNo: ["", Validators.required],
      userName: ["", Validators.required],
      password: ["", Validators.required]
    })
  }

  signUp() {
    const params = {
      firstName: this.userForm.controls['firstName'].value,
      middleName: this.userForm.controls['middleName'].value,
      lastName: this.userForm.controls['lastName'].value,
      emailAddress: this.userForm.controls['emailAddress'].value,
      contactNo: this.userForm.controls['contactNo'].value,
      userName: this.userForm.controls['userName'].value,
      password: this.userForm.controls['password'].value
    }

    this.appService.userRegister(params).subscribe(res => {
      alert('Successfully added!')
      this.router.navigate(['/user-login']);
    }, err => {
      alert(err.error);
    })
  }

}
