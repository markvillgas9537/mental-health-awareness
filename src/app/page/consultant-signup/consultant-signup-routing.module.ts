import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultantSignupPage } from './consultant-signup.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultantSignupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultantSignupPageRoutingModule {}
