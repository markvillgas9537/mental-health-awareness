import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsultantSignupPageRoutingModule } from './consultant-signup-routing.module';

import { ConsultantSignupPage } from './consultant-signup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsultantSignupPageRoutingModule
  ],
  declarations: [ConsultantSignupPage]
})
export class ConsultantSignupPageModule {}
