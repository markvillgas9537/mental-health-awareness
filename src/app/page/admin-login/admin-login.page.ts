import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.page.html',
  styleUrls: ['./admin-login.page.scss'],
})
export class AdminLoginPage implements OnInit {

  constructor(private router: Router, private appService: AppService) { }

  ngOnInit() {
  }

  login() {
    this.appService.showLoading();
    localStorage.setItem('userRole', JSON.stringify({ role: 'admin' }));
    this.appService.setCurrentUserRole(JSON.parse(JSON.parse(JSON.stringify(localStorage.getItem('userRole')))));
    this.router.navigate(['/admin-information']);
  }

}
