import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsultantInformationPageRoutingModule } from './consultant-information-routing.module';

import { ConsultantInformationPage } from './consultant-information.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsultantInformationPageRoutingModule
  ],
  declarations: [ConsultantInformationPage]
})
export class ConsultantInformationPageModule {}
