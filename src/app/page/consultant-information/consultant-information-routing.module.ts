import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultantInformationPage } from './consultant-information.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultantInformationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultantInformationPageRoutingModule {}
