import { Component, OnInit, ViewChild } from '@angular/core';
import { IonModal } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core/components';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-consultant-information',
  templateUrl: './consultant-information.page.html',
  styleUrls: ['./consultant-information.page.scss'],
})
export class ConsultantInformationPage implements OnInit {
  @ViewChild(IonModal) consultantInfoModal: IonModal;
  @ViewChild(IonModal) consultantAccountModal: IonModal;

  public isOpenConsultantInfo: boolean = false;
  public isOpenConsultantAccount: boolean = false;
  public message = 'This modal example uses triggers to automatically open a modal when the button is clicked.';
  public name: string;

  constructor(public appService: AppService) { }

  ngOnInit() {
  }

  modalClick(type: string, isOpen: boolean = false) {
    if (type === 'consultant-info') {
      this.isOpenConsultantInfo = isOpen;
    }

    if (type === 'consultant-account') {
      this.isOpenConsultantAccount = isOpen;
    }
  }

  confirm(type: string) {
    if (type === 'consultant-info') {
      this.isOpenConsultantInfo = false;
    }

    if (type === 'consultant-account') {
      this.isOpenConsultantAccount = false;
    }
  }

  onWillDismiss(event: Event) {
    const ev = event as CustomEvent<OverlayEventDetail<string>>;
    if (ev.detail.role === 'confirm') {
      this.message = `Hello, ${ev.detail.data}!`;
    }
  }
}
