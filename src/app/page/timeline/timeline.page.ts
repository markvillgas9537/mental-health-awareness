import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.page.html',
  styleUrls: ['./timeline.page.scss'],
})
export class TimelinePage implements OnInit {

  constructor(public appService: AppService) { }

  ngOnInit() {
  }

}
