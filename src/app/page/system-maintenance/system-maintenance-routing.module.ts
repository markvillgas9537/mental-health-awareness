import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SystemMaintenancePage } from './system-maintenance.page';

const routes: Routes = [
  {
    path: '',
    component: SystemMaintenancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemMaintenancePageRoutingModule {}
