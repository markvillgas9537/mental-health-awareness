import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SystemMaintenancePageRoutingModule } from './system-maintenance-routing.module';

import { SystemMaintenancePage } from './system-maintenance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SystemMaintenancePageRoutingModule
  ],
  declarations: [SystemMaintenancePage]
})
export class SystemMaintenancePageModule {}
