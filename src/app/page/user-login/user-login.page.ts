import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.page.html',
  styleUrls: ['./user-login.page.scss'],
})
export class UserLoginPage implements OnInit {
  public userLoginForm: FormGroup;

  constructor(
    private appService: AppService, 
    private fb: FormBuilder, 
    private router: Router
  ) { }

  ngOnInit() {
    this.userLoginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  login() {
    // const params = {
    //   username: this.userLoginForm.controls['username'].value,
    //   password: this.userLoginForm.controls['password'].value
    // };

    // this.appService.userLogin(params).subscribe(res => {
    //   localStorage.setItem('token', res.token)
    //   this.router.navigate(['/user-information']);
    // }, err => {
    //   alert(err.error);
    // })

    this.appService.showLoading();

    localStorage.setItem('userRole', JSON.stringify({ role: 'user' }));
    this.appService.setCurrentUserRole(JSON.parse(JSON.parse(JSON.stringify(localStorage.getItem('userRole')))));
    this.appService.navigateTo('/timeline');
  }

}
