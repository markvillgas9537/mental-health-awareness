import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.page.html',
  styleUrls: ['./user-list.page.scss'],
})
export class UserListPage implements OnInit {
  public userList: any = [];

  constructor(public appService: AppService, private router: Router) { }

  ngOnInit() {
    this.getUserList();
  }

  getUserList() {
    this.appService.getUser().subscribe(data => {
      this.userList = data;
    }, err => {
      if (err instanceof HttpErrorResponse) {
        if (err.status === 401) {
          this.router.navigate(['/login']);
        }
      }
    }, () => {

    })
  }

}
