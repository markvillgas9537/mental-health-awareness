import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public appService: AppService) { }

  ngOnInit() {
    this.appService.showLoading();
    localStorage.removeItem('userRole');
    this.appService.setCurrentUserRole(JSON.parse(JSON.parse(JSON.stringify(localStorage.getItem('userRole')))));
  }

}
