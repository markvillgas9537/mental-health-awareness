import { Component, OnInit, ViewChild } from '@angular/core';
import { IonModal } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core/components';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-user-information',
  templateUrl: './user-information.page.html',
  styleUrls: ['./user-information.page.scss'],
})
export class UserInformationPage implements OnInit {
  @ViewChild(IonModal) userInfoModal: IonModal;
  @ViewChild(IonModal) userAccountModal: IonModal;

  public isOpenUserInfo: boolean = false;
  public isOpenUserAccount: boolean = false;
  public message = 'This modal example uses triggers to automatically open a modal when the button is clicked.';
  public name: string;

  constructor(public appService: AppService) { }

  ngOnInit() {
  }

  modalClick(type: string, isOpen: boolean = false) {
    if (type === 'user-info') {
      this.isOpenUserInfo = isOpen;
    }

    if (type === 'user-account') {
      this.isOpenUserAccount = isOpen;
    }
  }

  confirm(type: string) {
    if (type === 'user-info') {
      this.isOpenUserInfo = false;
    }

    if (type === 'user-account') {
      this.isOpenUserAccount = false;
    }
  }

  onWillDismiss(event: Event) {
    const ev = event as CustomEvent<OverlayEventDetail<string>>;
    if (ev.detail.role === 'confirm') {
      this.message = `Hello, ${ev.detail.data}!`;
    }
  }

}
