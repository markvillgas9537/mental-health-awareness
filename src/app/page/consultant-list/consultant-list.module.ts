import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConsultantListPageRoutingModule } from './consultant-list-routing.module';

import { ConsultantListPage } from './consultant-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsultantListPageRoutingModule
  ],
  declarations: [ConsultantListPage]
})
export class ConsultantListPageModule {}
