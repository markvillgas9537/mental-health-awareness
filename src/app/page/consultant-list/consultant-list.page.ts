import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-consultant-list',
  templateUrl: './consultant-list.page.html',
  styleUrls: ['./consultant-list.page.scss'],
})
export class ConsultantListPage implements OnInit {

  constructor(public appService: AppService) { }

  ngOnInit() {
  }

}
