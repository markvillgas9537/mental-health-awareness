import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultantListPage } from './consultant-list.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultantListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultantListPageRoutingModule {}
