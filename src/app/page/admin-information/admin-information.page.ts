import { Component, OnInit, ViewChild } from '@angular/core';
import { IonModal } from '@ionic/angular';
import { OverlayEventDetail } from '@ionic/core/components';

@Component({
  selector: 'app-admin-information',
  templateUrl: './admin-information.page.html',
  styleUrls: ['./admin-information.page.scss'],
})
export class AdminInformationPage implements OnInit {
  @ViewChild(IonModal) adminInfoModal: IonModal;
  @ViewChild(IonModal) adminAccountModal: IonModal;

  public isOpenAdminInfo: boolean = false;
  public isOpenAdminAccount: boolean = false;
  public message = 'This modal example uses triggers to automatically open a modal when the button is clicked.';
  public name: string;

  constructor() { }

  ngOnInit() {
  }

  modalClick(type: string, isOpen: boolean = false) {
    if (type === 'admin-info') {
      this.isOpenAdminInfo = isOpen;
    }

    if (type === 'admin-account') {
      this.isOpenAdminAccount = isOpen;
    }
  }

  confirm(type: string) {
    if (type === 'admin-info') {
      this.isOpenAdminInfo = false;
    }

    if (type === 'admin-account') {
      this.isOpenAdminAccount = false;
    }
  }

  onWillDismiss(event: Event) {
    const ev = event as CustomEvent<OverlayEventDetail<string>>;
    if (ev.detail.role === 'confirm') {
      this.message = `Hello, ${ev.detail.data}!`;
    }
  }
}
