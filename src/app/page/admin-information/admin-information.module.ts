import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminInformationPageRoutingModule } from './admin-information-routing.module';

import { AdminInformationPage } from './admin-information.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminInformationPageRoutingModule
  ],
  declarations: [AdminInformationPage]
})
export class AdminInformationPageModule {}
