import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminInformationPage } from './admin-information.page';

const routes: Routes = [
  {
    path: '',
    component: AdminInformationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminInformationPageRoutingModule {}
