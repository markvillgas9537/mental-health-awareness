import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultantPagePage } from './consultant-page.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultantPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultantPagePageRoutingModule {}
