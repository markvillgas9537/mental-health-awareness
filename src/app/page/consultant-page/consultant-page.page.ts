import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/service/app/app.service';

@Component({
  selector: 'app-consultant-page',
  templateUrl: './consultant-page.page.html',
  styleUrls: ['./consultant-page.page.scss'],
})
export class ConsultantPagePage implements OnInit {

  constructor(
    public appService: AppService
  ) { }

  ngOnInit() {
  }
}
